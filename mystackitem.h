#ifndef _mystackitem_h
#define _mystackitem_h

#include <vector>
using namespace std;

typedef vector<double> vec;

typedef enum {
	NUMBER,
	VECTOR
} dataType;

typedef struct {
	dataType type;
	double d_value;
	vec v_value;
} myStackItem;

myStackItem makeDouble(double value);
myStackItem makeVector(vec value);

bool isDouble(myStackItem toTest);
bool isVector(myStackItem toTest);

double getDouble(myStackItem from);
vec getVector(myStackItem from);

#endif