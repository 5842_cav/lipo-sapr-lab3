teach-lex-polska-calc
=====================
Demo project for LEX.

Calucates expressions in reverse (polska) notations.

Supported:
1. doubles
	* 4 math operations (+, -, *, /)
2. double arrays
	* insertion to first position -> `[1 2] [3] +` `[1, 2] [3] +`
	* concatenation -> `[1 2] [3, 4] +` `[1, 2] [3, 4] +` `[1 2] [3 4] +`
	*insertion to last position -> `[1] [2 3] +` `[1] [2, 3] +`
