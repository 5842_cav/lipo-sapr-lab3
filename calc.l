%{
/*
This file is part of Reverse Notation Calc.

    Reverse Notation Calc is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

*/

#include <stack>
#include "mystackitem.h"

using namespace std;

void print_vector(FILE* stream, vec v);

double d_value;

typedef enum {
	LexNumber=1001,
	LexPlus,
	LexMinus,
	LexDiv,
	LexMult,
	LexEnd,
	LexUnknown,
	LexArray
} LexType;

stack<myStackItem> main_stack;

vec reading_now;
bool reading_vector;
bool vector_last_double;

int end_file;

%}

%s SKIPERROR

digit     [0-9]
sign      [\-\+]
number    {sign}?{digit}+(\.{digit}+)?

%%

{number}	{
				d_value=atof(yytext);
				if (reading_vector) {
					reading_now.push_back(d_value);
					vector_last_double = true;
				} else {
				fprintf(stderr, "found %f\n", d_value);
				return LexNumber;
				}
			}
\[		{
			if (reading_vector) {
				return LexUnknown;
			} else {
				reading_vector = true;
				vector_last_double = false;
			}
		}
\]		{
			if (!vector_last_double || !reading_vector) {
				return LexUnknown;
			} else {
				fprintf(stderr, "found an array\n");
				reading_vector = false;
				vector_last_double = false;
				return LexArray;
			}
		}
,		{
			if (!vector_last_double || !reading_vector) {
				return LexUnknown;
			} else {
				vector_last_double = false;
			}
		}
\+		{
			fprintf(stderr, "plus\n");
			return LexPlus; }
\-		{
			fprintf(stderr, "minus\n");
			return LexMinus; }
\/		{
			fprintf(stderr, "div\n");
			return LexDiv; }
\*		{
			fprintf(stderr, "mult\n");
			return LexMult; }
^[ \t]*\n	{
			fprintf(stderr, "empty line\n");
		}
\n		{
			fprintf(stderr, "CR\n");
			return LexEnd; }
[ \t]		{ }
.		{ return LexUnknown; }

<SKIPERROR>[^\n]* { reading_vector = false; vector_last_double = false; }
%%

int process_command(int token)
{
	fprintf(stderr, "token: %d\n", token);

	switch (token) {
	case LexNumber:
		main_stack.push(makeDouble(d_value));
		break;
	case LexArray: {
		vec temp;
		for (int i = 0; i < reading_now.size(); i++) {
			temp.push_back(reading_now[i]);
		}
		reading_now.clear();
		print_vector(stderr, temp);
		fprintf(stderr, "\n");
		main_stack.push(makeVector(temp));
		break;}
	case LexPlus: {
		if (main_stack.empty()) {
			return -1;
		}
		myStackItem a = main_stack.top();
		main_stack.pop();

		if (main_stack.empty()) {
			return -1;
		}
		myStackItem b = main_stack.top();
		main_stack.pop();
		
		if (isDouble(b) && isDouble(a)) {
			main_stack.push(makeDouble(getDouble(b) + getDouble(a)));
		}
		
		if (isVector(a) && isVector(b)) {
			vec va = getVector(a);
			vec vb = getVector(b);
			for (int i = 0; i < va.size(); i++) {
				vb.push_back(va[i]);
			}
			main_stack.push(makeVector(vb));
		}
		
		if (isVector(a) && isDouble(b)) {
			vec va = getVector(a);
			double db = getDouble(b);
			vec temp;
			temp.push_back(db);
			for (int i = 0; i < va.size(); i++) {
				temp.push_back(va[i]);
			}
			main_stack.push(makeVector(temp));
		}
		
		if (isDouble(a) && isVector(b)) {
			double da = getDouble(a);
			vec vb = getVector(b);
			vb.push_back(da);
			main_stack.push(makeVector(vb));
		}
		
		break;}
	case LexMinus: {
		if (main_stack.empty()) {
			return -1;
		}
		myStackItem a = main_stack.top();
		main_stack.pop();

		if (main_stack.empty()) {
			return -1;
		}
		myStackItem b = main_stack.top();
		main_stack.pop();

		if (!isDouble(a) && !isDouble(b)) {
			return -1;
		}
		
		main_stack.push(makeDouble(getDouble(b) - getDouble(a)));
		
		break;}
	case LexDiv: {
		if (main_stack.empty()) {
			return -1;
		}
		myStackItem a = main_stack.top();
		main_stack.pop();

		if (main_stack.empty()) {
			return -1;
		}
		myStackItem b = main_stack.top();
		main_stack.pop();

		if (!isDouble(a) && !isDouble(b)) {
			return -1;
		}
		
		main_stack.push(makeDouble(getDouble(b) / getDouble(a)));
		
		break;}
	case LexMult: {
		if (main_stack.empty()) {
			return -1;
		}
		myStackItem a = main_stack.top();
		main_stack.pop();

		if (main_stack.empty()) {
			return -1;
		}
		myStackItem b = main_stack.top();
		main_stack.pop();

		if (!isDouble(a) && !isDouble(b)) {
			return -1;
		}
		
		main_stack.push(makeDouble(getDouble(b) * getDouble(a)));
		
		break;}
	case LexEnd:
	case 0:
		return 0;
	case LexUnknown:
		reading_vector = false;
		vector_last_double = false;
		return -1;

	}
	return 1;
}

int calc_line()
{
	int token = yylex();
	if (token == 0) {
		return 1;
	}

	while (1) {
		int cmd_res = process_command(token);
		if (cmd_res == 0) {
			break;
		}
		else if (cmd_res == -1) {
			fprintf(stderr, "Syntax error\n");
			return 0;
		}
		token = yylex();
	}

	if (main_stack.empty()) {
		fprintf(stderr, "Stack is empty but required value\n");
		return 0;
	}

	myStackItem result = main_stack.top();
	main_stack.pop();
	if (isDouble(result)) {
		fprintf(yyout, "%f ", getDouble(result));
	}
	if (isVector(result)) {
		print_vector(yyout, getVector(result));
	}

	if (!main_stack.empty()) {
		fprintf(stderr, "Stack not empty after calculation\n");
		return 0;
	}

	return 1;
}

void calc()
{
	while (!end_file) {
		fprintf(stderr, "parse line\n");
		if (calc_line() == 0) {
			printf("FAIL\n");
			BEGIN(SKIPERROR);
			yylex();
			BEGIN(INITIAL);
		}
		else {
			printf("OK\n");
		}
		fprintf(stderr, "line parsed\n");
	}
}

int main(void)
{
	end_file = 0;
	calc();
	return 0;
}

int yywrap(void)
{
	end_file = 1;
	return 1;
}

void print_vector(FILE* stream, vec v)
{
	fprintf(stream, "array(");
	fprintf(stream, "%d, ", v.size());
	for (int i = 0; i < v.size()-1; i++) {
		fprintf(stream, "%f, ", v[i]);
	}
	fprintf(stream, "%f) ", v[v.size()-1]);
}
