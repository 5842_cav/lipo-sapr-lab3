# Makefile using Lex to build

CC     = g++
CFLAGS = -c

LEX    = flex
LFLAGS =

LN     = g++
LNFLAGS=

SRC    = calc.l
OBJ    = calc.o mystackitem.o

all:	calc

test: all
	./calc < test1.txt

calc:	$(OBJ)
	$(LN) $(LNFLAGS) -o calc $(OBJ)

calc.o:	calc.c
	$(CC) $(CFLAGS) -o calc.o calc.c

calc.c: calc.l
	$(LEX) $(LFLAGS) -o calc.c $(SRC)

mystackitem.o: mystackitem.cpp
	$(CC) $(CFLAGS) -o mystackitem.o mystackitem.cpp

clean:
	rm -f calc calc.c *.o
